﻿using System;
using System.IO;
using System.Linq;
using IWshRuntimeLibrary;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Management;


namespace SimpleCopy
{
   static class SimpleCopy
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo culture = System.Globalization.CultureInfo.CurrentCulture;
            InputArguments arguments = getParameters(args);
            string srcFolderName = resolvePath(arguments["srcFolder"]);
            string dstFolderName = resolvePath(arguments["dstFolder"]);
            string srcFileName = resolvePath(arguments["srcFile"]);
            string dstFileName = resolvePath(arguments["dstFileName"]);
            string lnkTarget = resolvePath(arguments["lnkTarget"]);
            string executeFile = resolvePath(arguments["executeFile"]);
            bool removeDst = arguments.Contains("removeDst");
            bool removeSelfLnk = arguments.Contains("removeSelfLnk");
            bool overwriteFiles = arguments.Contains("overwriteFiles");
            bool closeProcess = arguments.Contains("closeProcess");
            string MainMessage = resolvePath(arguments["MainMessage"]);
            string RobocopyArgs = arguments["RobocopyArgs"];
            bool ErrorHandled = false;
            Console.WriteLine(MainMessage);
            try
            {
                closeProcessByPath(dstFolderName, closeProcess);
                //closeProcessByPath(dstFileName, closeProcess);
                //MyCopyFolder(srcFolderName, dstFolderName, removeDst, overwriteFiles);
                //FileCopy(srcFileName, dstFileName, overwriteFiles);
                StartRobocopy(srcFolderName, dstFolderName, RobocopyArgs);
                CreateShortcutOnDesktop(lnkTarget);
                if (System.IO.File.Exists(executeFile))
                {
                    Console.WriteLine($"Launch file {executeFile}");
                    System.Diagnostics.Process.Start(executeFile);
                }
                RemoveSelfShortcut(removeSelfLnk);
            }
            catch (Exception e)
            {
                ErrorHandled = true;
                throw (e);
            }
            finally
            {
                if (ErrorHandled == false)
                {
                    Environment.Exit(0);
                }

                if (culture.IetfLanguageTag.Equals(@"ru-RU"))
                {
                    Console.WriteLine(@"Если вы видите данное сообщение, скопируйте весь текст в этом окне и отправьте это вашему системному администратору.");
                    Console.WriteLine(@"Нажмите любую кнопку для закрытия этого окна.");
                }
                else
                {
                    Console.WriteLine(@"If you see this message, copy all the text in this window and send it to your system administrator.");
                    Console.WriteLine(@"Press any button to close this window.");
                }
                Console.ReadKey();
            }
        }
        private static InputArguments getParameters(string[] args)
        {
            if (Array.Exists(args, s => s.Contains("-parameterFilePath")))
            {
                InputArguments arguments = new InputArguments(args);
                string parameterFilePath = resolvePath(arguments["parameterFilePath"]);
                InputArguments parameterFile = new InputArguments(System.IO.File.ReadAllLines(parameterFilePath));
                return parameterFile;
            }
            else
            {
                InputArguments arguments = new InputArguments(args);
                return arguments;
            }
        }
        private static void closeProcessByPath(string path, bool confirm)
        {
            if (confirm & !string.IsNullOrEmpty(path))
            {
                if (Directory.Exists(path))
                {
                    string[] ExeFilesEntries = Directory.GetFiles(path, "*.exe");
                    Parallel.ForEach(ExeFilesEntries, ExeFilePath =>
                    {
                        string FileBaseName = Path.GetFileNameWithoutExtension(ExeFilePath);
                        Process[] processEntries = Process.GetProcessesByName(FileBaseName);
                        Parallel.ForEach(processEntries, (proces, ParallelLoopState) =>
                        {
                            try
                            {
                                /*Проверка доступности свойства у процесса. Если ошибка доступа, то пользователь не владелец процесса.*/
                                string processFileName = proces.MainModule.FileName;
                            }
                            catch
                            {
                                ParallelLoopState.Stop();
                                return;
                            }
                            try
                            {
                                if (proces.MainModule.FileName.Contains(path))
                                {
                                    Console.WriteLine($"Path: {path}, MainModulePath:{proces.MainModule.FileName}");
                                    proces.EnableRaisingEvents = true;
                                    proces.Kill();
                                    proces.WaitForExit();
                                    System.Threading.Thread.Sleep(1000);
                                    Console.WriteLine($"Process {proces.ProcessName} closed");
                                }
                            }
                            catch
                            {
                                throw (new SystemException($"An error occurred when closing the process {proces.ProcessName}"));
                            }
                        });

                    });
                }
            }
        }
        private static string GetRobocopyArgsString(string RobocopyArgs)
        {
            char[] charSeparators = new char[] { ' ' };
            string[] ArgsArray = RobocopyArgs.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
            System.Text.StringBuilder myStringBuilder = new System.Text.StringBuilder();
            foreach (string arg in ArgsArray)
            {
                myStringBuilder.Append($"\"{arg.Trim()}\" ");
            }
            return myStringBuilder.ToString().Trim();
        }
        #region Copy Region
        private static void StartRobocopy(string SrcPath, string Dstpath, string RobocopyArgs)
        {
            Process p = new Process();
            ProcessStartInfo psi = new ProcessStartInfo();
            psi.Arguments = $"\"{SrcPath}\" \"{Dstpath}\" {GetRobocopyArgsString(RobocopyArgs)}";
            psi.FileName = "Robocopy.exe";
            psi.UseShellExecute = false;
            p.StartInfo = psi;
            p.Start();
            p.WaitForExit();
            if (p.ExitCode > 3)
            {

                throw (new SystemException($"Robocopy error. Exit code {p.ExitCode}"));
            }
        }
        private static void MyCopyFolder(string srcFolderName, string dstFolderName, bool removeDst, bool overwriteFiles)
        {
            if (!string.IsNullOrEmpty(srcFolderName) & !string.IsNullOrEmpty(dstFolderName))
            {
                if (!Directory.Exists(srcFolderName))
                {
                    Console.WriteLine($"Source directory does not exist or could not be found {srcFolderName}");
                }
                else
                {
                    if (removeDst & Directory.Exists(dstFolderName))
                    {
                        Console.WriteLine($"Directory {dstFolderName} exsists. Directory will be deleted.");
                        Directory.Delete(dstFolderName, true);
                    }
                    if (overwriteFiles & Directory.Exists(dstFolderName))
                    {
                        Console.WriteLine($"Directory {dstFolderName} exsists. Files in the directory will be overwritten");
                    }
                    Console.WriteLine($"Start Copy from {srcFolderName} to {dstFolderName}");
                    DirectoryCopy(srcFolderName, dstFolderName, true, overwriteFiles);
                }
            }
        }
        private static void FileCopy(string srcFileName, string dstFileName, bool overWriteFile)
        {
            if (!string.IsNullOrEmpty(srcFileName) & !string.IsNullOrEmpty(dstFileName))
            {
                if (!System.IO.File.Exists(srcFileName))
                {
                    Console.WriteLine($"Source file does not exist or could not be found {srcFileName}");
                }
                else
                {
                    if (overWriteFile)
                    {
                        Console.WriteLine($"File {dstFileName} exsists. OverWrite");
                        //System.IO.File.Delete(dstFileName);
                    }
                    Console.WriteLine($"Start copy file from {srcFileName} to {dstFileName}");
                    System.IO.File.Copy(srcFileName, dstFileName, overWriteFile);
                }
            }
        }
        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs, bool overwriteFiles)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                Console.WriteLine(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
                return;
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, overwriteFiles);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs, overwriteFiles);
                }
            }

        }
        #endregion
        #region Shortcut Region
        private static string GetCommandLine(this Process process)
        {
            using (ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT CommandLine FROM Win32_Process WHERE ProcessId = " + process.Id))
            using (ManagementObjectCollection objects = searcher.Get())
            {
                return objects.Cast<ManagementBaseObject>().SingleOrDefault()?["CommandLine"]?.ToString();
            }

        }
        private static void RemoveSelfShortcut(bool Confirm)
        {
            if (Confirm)
            {
                object shDesktop = (object)"Desktop";
                WshShell shell = new WshShell();
                Process SelfProcess = Process.GetCurrentProcess();
                string selfProcessPath = SelfProcess.MainModule.FileName;
                string[] fileEntries = System.IO.Directory.GetFiles((string)shell.SpecialFolders.Item(ref shDesktop), "*.lnk");
                string SelfCmdLine = GetCommandLine(SelfProcess);
                foreach (string fileName in fileEntries)
                {
                    IWshRuntimeLibrary.IWshShortcut Shortcut = GetShortcut(fileName);
                    if (Shortcut.TargetPath.Contains(SelfProcess.MainModule.FileName) & SelfCmdLine.Contains(Shortcut.Arguments))
                    {
                        Console.WriteLine($"Remove {fileName}");
                        System.IO.File.Delete(fileName);
                        Shell32Api.RefreshDesktop();
                    }
                }
            }
        }
        static IWshRuntimeLibrary.IWshShortcut GetShortcut(string filePath)
        {
            IWshRuntimeLibrary.WshShell shell = new IWshRuntimeLibrary.WshShell();
            try
            {
                IWshRuntimeLibrary.IWshShortcut shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(filePath);
                return shortcut;
            }
            catch (System.Runtime.InteropServices.COMException)
            {
                return null;
            }
        }
        private static void CreateShortcutOnDesktop(string lnkTarget)
        {
            if (System.IO.File.Exists(lnkTarget) & !string.IsNullOrEmpty(lnkTarget))
            {
                Console.WriteLine($"Create lnk. Target path: {lnkTarget}");
                object shDesktop = (object)"Desktop";
                WshShell shell = new WshShell();
                string lnkName = Path.GetFileNameWithoutExtension(lnkTarget);
                string shortcutAddress = (string)shell.SpecialFolders.Item(ref shDesktop) + $"\\{lnkName}.lnk";
                IWshShortcut shortcut = (IWshShortcut)shell.CreateShortcut(shortcutAddress);
                shortcut.TargetPath = lnkTarget;
                shortcut.Save();
                Shell32Api.RefreshDesktop();
            }
        }
        #endregion
        #region Path resolving
        private static string resolvePath(string Path)
        {
            string resolvedPath = @"";
            if (!string.IsNullOrEmpty(Path))
            {
                resolvedPath = Environment.ExpandEnvironmentVariables(Path);
            }
            return resolvedPath;
        }
        private static string ResolveDfsPathToSmbPath(string DfsPath)
        {
            try
            {
                Win32Api.DFS_STORAGE_INFO[] dFS_STORAGE_INFO = Win32Api.NetDfsGetClientInfo(DfsPath);
                string[] SplitPath = DfsPath.Split(new string[] { dFS_STORAGE_INFO[0].ShareName }, 2, StringSplitOptions.None);
                string SmbPath = $"\\\\{dFS_STORAGE_INFO[0].ServerName}\\{dFS_STORAGE_INFO[0].ShareName}{SplitPath[1]}";
                return SmbPath;
            }
            catch
            {
                return DfsPath;
            }
        }
        #endregion
    }
}
public class Shell32Api
{
    [DllImport("Shell32.dll")]
    private static extern int SHChangeNotify(int eventId, int flags, IntPtr item1, IntPtr item2);
    public static void RefreshDesktop()
    {
        SHChangeNotify(0x8000000, 0x1000, IntPtr.Zero, IntPtr.Zero);
    }
}
public class Win32Api
{
   
    [DllImport("netapi32.dll", SetLastError = true)]
    private static extern int NetApiBufferFree(IntPtr buffer);
    [DllImport("Netapi32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
    private static extern int NetDfsGetClientInfo
    (
    [MarshalAs(UnmanagedType.LPWStr)] string EntryPath,
    [MarshalAs(UnmanagedType.LPWStr)] string ServerName,
    [MarshalAs(UnmanagedType.LPWStr)] string ShareName,
    int Level,
    ref IntPtr Buffer
    );
    public struct DFS_INFO_3
    {
        [MarshalAs(UnmanagedType.LPWStr)]
        public string EntryPath;
        [MarshalAs(UnmanagedType.LPWStr)]
        public string Comment;
        public UInt32 State;
        public UInt32 NumberOfStorages;
        public IntPtr Storages;
    }
    public struct DFS_STORAGE_INFO
    {
        public Int32 State;
        [MarshalAs(UnmanagedType.LPWStr)]
        public string ServerName;
        [MarshalAs(UnmanagedType.LPWStr)]
        public string ShareName;
    }
    public static DFS_STORAGE_INFO[] NetDfsGetClientInfo(string DfsPath)
    {
        IntPtr buffer = new IntPtr();

        try
        {

            int result = NetDfsGetClientInfo(DfsPath, null, null, 3, ref buffer);
            if (result != 0)
            {
                throw (new SystemException("Error getting DFS information"));
            }
            else
            {

                DFS_INFO_3 dfsInfo = (DFS_INFO_3)Marshal.PtrToStructure(buffer, typeof(DFS_INFO_3));
                DFS_STORAGE_INFO[] dFS_STORAGE_INFOs = new DFS_STORAGE_INFO[dfsInfo.NumberOfStorages];
                for (int i = 0; i < dfsInfo.NumberOfStorages; i++)
                {
                    IntPtr storage = new IntPtr(dfsInfo.Storages.ToInt64() + i * Marshal.SizeOf(typeof(DFS_STORAGE_INFO)));
                    DFS_STORAGE_INFO storageInfo = (DFS_STORAGE_INFO)Marshal.PtrToStructure(storage, typeof(DFS_STORAGE_INFO));
                    dFS_STORAGE_INFOs.SetValue(storageInfo, i);
                }
                return dFS_STORAGE_INFOs;
            }
        }
        catch (Exception e)
        {
            throw (e);
        }
        finally
        {
            NetApiBufferFree(buffer);
        }
    }
}

public class InputArguments
{
    #region fields & properties
    public const string DEFAULT_KEY_LEADING_PATTERN = "-";

    protected Dictionary<string, string> _parsedArguments = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
    protected readonly string _keyLeadingPattern;

    public string this[string key]
    {
        get { return GetValue(key); }
        set
        {
            if (key != null)
                _parsedArguments[key] = value;
        }
    }
    public string KeyLeadingPattern
    {
        get { return _keyLeadingPattern; }
    }
    #endregion

    #region public methods
    public InputArguments(string[] args, string keyLeadingPattern)
    {
        _keyLeadingPattern = !string.IsNullOrEmpty(keyLeadingPattern) ? keyLeadingPattern : DEFAULT_KEY_LEADING_PATTERN;

        if (args != null && args.Length > 0)
            Parse(args);
    }
    public InputArguments(string[] args) : this(args, null)
    {
    }

    public bool Contains(string key)
    {
        string adjustedKey;
        return ContainsKey(key, out adjustedKey);
    }

    public virtual string GetPeeledKey(string key)
    {
        return IsKey(key) ? key.Substring(_keyLeadingPattern.Length) : key;
    }
    public virtual string GetDecoratedKey(string key)
    {
        return !IsKey(key) ? (_keyLeadingPattern + key) : key;
    }
    public virtual bool IsKey(string str)
    {
        return str.StartsWith(_keyLeadingPattern);
    }
    #endregion

    #region internal methods
    protected virtual void Parse(string[] args)
    {
        for (int i = 0; i < args.Length; i++)
        {
            if (args[i] == null) continue;

            string key = null;
            string val = null;

            if (IsKey(args[i]))
            {
                key = args[i];

                if (i + 1 < args.Length && !IsKey(args[i + 1]))
                {
                    val = args[i + 1];
                    i++;
                }
            }
            else
                val = args[i];

            // adjustment
            if (key == null)
            {
                key = val;
                val = null;
            }
            _parsedArguments[key] = val;
        }
    }

    protected virtual string GetValue(string key)
    {
        string adjustedKey;
        if (ContainsKey(key, out adjustedKey))
            return _parsedArguments[adjustedKey];

        return null;
    }

    protected virtual bool ContainsKey(string key, out string adjustedKey)
    {
        adjustedKey = key;

        if (_parsedArguments.ContainsKey(key))
            return true;

        if (IsKey(key))
        {
            string peeledKey = GetPeeledKey(key);
            if (_parsedArguments.ContainsKey(peeledKey))
            {
                adjustedKey = peeledKey;
                return true;
            }
            return false;
        }

        string decoratedKey = GetDecoratedKey(key);
        if (_parsedArguments.ContainsKey(decoratedKey))
        {
            adjustedKey = decoratedKey;
            return true;
        }
        return false;
    }
    #endregion
}